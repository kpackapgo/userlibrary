package com.ee.userlibrary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    @Override
    public List<User> fetchAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void createUser(User user) {
        validateName(user.getFirstName());
        validateName(user.getLastName());
        validateEmail(user.getEmail());
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    private boolean validateEmail(String email) {
        if(email == null) {
            throw  new InvalidEmailFormatException();
        }
        if(!email.matches("^[A-Za-z0-9+_.-]+@(.+)$")) {
            throw new InvalidEmailFormatException();
        }
        Optional<User> optionalUser = userRepository.findByEmail(email);

        if(optionalUser.isPresent()) {
            throw new EmailAlreadyExistsException();
        }
        return true;
    }

    private boolean validateName(String name) {
        if (name == null || !name.matches("[A-Za-z-,]{2,20}")) {
            throw new InvalidNameFormatException();
        }
        return true;
    }
}
