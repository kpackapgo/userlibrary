package com.ee.userlibrary;

public class EmailAlreadyExistsException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Email already exists.";
    }
}
