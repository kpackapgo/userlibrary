package com.ee.userlibrary;

public class InvalidEmailFormatException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Email format is invalid.";
    }
}
