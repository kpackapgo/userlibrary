package com.ee.userlibrary;

public class InvalidNameFormatException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Name format is invalid.";
    }
}
