package com.ee.userlibrary;

import java.util.List;

public interface UserService {

    List<User> fetchAllUsers();

    void createUser(User user);

    void deleteUser(Long id);
}
