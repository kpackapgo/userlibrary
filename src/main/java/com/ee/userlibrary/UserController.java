package com.ee.userlibrary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping(value = "user-api")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "load-all")
    public List<User> getAllUsers() {
        List<User> users = userService.fetchAllUsers();
        if(users.isEmpty()) {
            userService.createUser(new User("Aleksander", "Ivanov", "ai@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Konstantin", "Velikov", "konstantinv@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Grigor", "Dimitrov", "grigord@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Kubrat", "Pulev", "kubratp@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Dimitrii", "Georgiev", "dimitriig@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Elica", "Perova", "elicap@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Joana", "Semova", "zetsa@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Georgi", "Georgiev", "grandoman@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Petar", "Petkov", "kliukliu@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Petar", "Ivanov", "talkmuch@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Dragomir", "Nachev", "stealer@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Zina", "Filipova", "princesswarrior@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Krasimir", "Dimitrov", "555@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Mladina", "Yaneva", "sexy_ina@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Mladina", "Dimitrova", "bulka@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Kristal", "Deleva", "dancert@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Ulian", "Grozdev", "powerpunch@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Nadejda", "Kolchakova", "nadq_lqstovicata@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Simona", "Arzimova", "saleta@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Vasil", "Hadjiev", "batmando@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Tom", "Cruise", "andjerry@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Chris", "Evans", "oracle@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Sergei", "Believ", "rusboy@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Plamena", "Jabilova", "kaka_p@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Kristina", "Panaiotova-Enova", "crystal@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Maksim", "Tehandov", "stripman@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("John", "Snow", "winteriscoming@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Natsu", "Dragneel", "dragonslayer@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Grandina", "Panteva", "dragon@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Marica", "Qntrova", "river_party@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Kapriz", "Shatrova", "character@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Qsen", "Zverev", "clear_sky@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Roman", "Djokovich", "rome@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Natasha", "Romanov", "spy1@gmail.com", new Date(1999, 10, 2)));
            userService.createUser(new User("Natasha", "Romanov", "spy2@gmail.com", new Date(1999, 10, 2)));
            users = userService.fetchAllUsers();
            }
        return users;
    }

    @PostMapping(value = "create")
    public void createUser(@RequestBody User user) {
        userService.createUser(user);
    }

    @DeleteMapping(path ={"/{id}"})
    public void delete(@PathVariable("id") Long id) {
        userService.deleteUser(id);
    }
}
