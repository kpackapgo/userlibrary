package com.ee.userlibrary;

import com.ee.userlibrary.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.junit.jupiter.api.Test;

import java.sql.Date;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = UserlibraryApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void userIsCreated() throws Exception {
        User user = new User("Natsu", "Dragneel", "dragonslayer@fairytail.com", new Date(System.currentTimeMillis()));
        ObjectMapper mapper = new ObjectMapper();
        String userAsJson = mapper.writeValueAsString(user);
        mockMvc.perform(post("/user-api/create").contentType(MediaType.APPLICATION_JSON).content(userAsJson)).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void userWithEmailThatIsAlreadyExistingReturnsConflict() throws Exception {
        User user = new User("Natsu", "Dragneel", "dragonslayer@fairytail.com", new Date(System.currentTimeMillis()));
        ObjectMapper mapper = new ObjectMapper();
        String userAsJson = mapper.writeValueAsString(user);
        doThrow(new EmailAlreadyExistsException()).when(userService).createUser(user);
        mockMvc.perform(post("/user-api/create").contentType(MediaType.APPLICATION_JSON).content(userAsJson)).andDo(print()).andExpect(status().isConflict());
    }

    @Test
    public void userWithInvalidEmailReturnsBadRequest() throws Exception {
        User user = new User("Natsu", "Dragneel", "sdaads@fairytail.com", new Date(System.currentTimeMillis()));
        ObjectMapper mapper = new ObjectMapper();
        String userAsJson = mapper.writeValueAsString(user);
        doThrow(new InvalidEmailFormatException()).when(userService).createUser(user);
        mockMvc.perform(post("/user-api/create").contentType(MediaType.APPLICATION_JSON).content(userAsJson)).andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    public void userWithInvalidNameWillReturnBadRequest() throws Exception {
        User user = new User("Natsu", "Dragneel", "@fairytail.com", new Date(System.currentTimeMillis()));
        ObjectMapper mapper = new ObjectMapper();
        String userAsJson = mapper.writeValueAsString(user);
        doThrow(new InvalidNameFormatException()).when(userService).createUser(user);
        mockMvc.perform(post("/user-api/create").contentType(MediaType.APPLICATION_JSON).content(userAsJson)).andDo(print()).andExpect(status().isBadRequest());
    }
}
