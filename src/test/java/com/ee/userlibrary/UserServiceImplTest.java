package com.ee.userlibrary;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.junit.jupiter.MockitoExtension;


import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    public void setUp() {
        userService = new UserServiceImpl(userRepository);
    }

    @Test
    public void userIsSavedSuccessfully () {

        User user = new User("Julius", "Novachrono", "magic.king@gmail.com", new Date(System.currentTimeMillis()));

        userService.createUser(user);

        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void userWithExistingEmailIsNotSaved() {

        User user = new User("Julius", "Novachrono", "magic.king@gmail.com", new Date(System.currentTimeMillis()));
        User user2 = new User("Bam", "Shinsu", "magic.king@gmail.com", new Date(System.currentTimeMillis()));

        userService.createUser(user);

        when(userRepository.findByEmail("magic.king@gmail.com")).thenReturn(Optional.of(user));

        assertThrows(EmailAlreadyExistsException.class, () -> {
            userService.createUser(user2);
        });

        verify(userRepository, never()).save(user2);
    }

    @Test
    public void userWithIncorrectEmailIsNotSaved() {
        User user = new User("Julius", "Novachrono", "@gmail.com", new Date(System.currentTimeMillis()));

        assertThrows(InvalidEmailFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }

    @Test
    public void userWithoutEmailIsNotSaved() {
        User user = new User();
        user.setFirstName("Julius");
        user.setLastName("Novachrono");

        assertThrows(InvalidEmailFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }

    @Test
    public void userWithEmptyEmailIsNotSaved() {
        User user = new User("Julius", "Novachrono", "", new Date(System.currentTimeMillis()));

        assertThrows(InvalidEmailFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }


    @Test
    public void userWithIncorrectFirstNameIsNotSaved() {
        User user = new User("Julius7", "Novachrono", "magic.king@gmail.com", new Date(System.currentTimeMillis()));

        assertThrows(InvalidNameFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }

    @Test
    public void userWithoutFirstNameIsNotSaved() {
        User user = new User();
        user.setLastName("Novachrono");
        user.setEmail("magic.king@gmail.com");

        assertThrows(InvalidNameFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }

    @Test
    public void userWithIncorrectLastNameIsNotSaved() {
        User user = new User("Julius", "Novachrono007", "magic.king@gmail.com", new Date(System.currentTimeMillis()));

        assertThrows(InvalidNameFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }

    @Test
    public void userWithoutLastNameIsNotSaved() {
        User user = new User();
        user.setFirstName("Julius");
        user.setEmail("magic.king@gmail.com");

        assertThrows(InvalidNameFormatException.class, () -> {
            userService.createUser(user);
        });

        verify(userRepository, never()).save(user);
    }

}